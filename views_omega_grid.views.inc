<?php
/**
 * @file
 * Settings for the style of this plugin.
 */

/**
 * Implements hook_views_plugins().
 */
function views_omega_grid_views_plugins() {
  return array(
    'style' => array(
      'omega_grid' => array(
        'title' => t('Omega Grid'),
        'help' => t('Assign a css class to the view. The grid has to be built by the theme.'),
        'help topic' => 'style-omega-grid',
        'handler' => 'views_omega_grid_style_plugin',
        'uses options' => TRUE,
        'uses row plugin' => TRUE,
        'uses row class' => TRUE,
        'type' => 'normal',
        'theme' => 'views_view_omega_grid',
      ),
    ),
  );
}
