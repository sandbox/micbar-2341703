<?php
/**
 * @file
 * Default simple view template to display a rows in a responsive grid.
 *
 * - $rows contains a nested array of rows. Each row contains an array of
 *   columns.
 * - $row_classes contains an array of row classes indexed by the row number. Each class contains an
 *   array of class names.
 * - $classes contains an array of class names for the wrapper div.
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)) : ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<<?php print $wrapper ?> class="<?php print $classes; ?>">
  <?php foreach ($rows as $num => $row) : ?>
    <<?php print $item_list ?> <?php if ($row_classes[$num]) { print ' class="' . implode(' ', $row_classes[$num]) . '"'; } ?>>
          <?php print $row; ?>
    </<?php print $item_list ?>>
  <?php endforeach; ?>
</<?php print $wrapper ?>>
