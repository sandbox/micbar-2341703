<?php
/**
 * @file
 * Contains the responsive grid style plugin.
 */

/**
 * Style plugin to render each item in a responsive grid.
 *
 * @ingroup views_style_plugins
 */
class views_omega_grid_style_plugin extends views_plugin_style {
  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['wrapper_classes'] = array('default' => 'views--omega-grid');
    $options['row_class'] = array('default' => 'views--omega-row');
    $options['default_classes'] = array('default' => 0);
    $options['switch'] = array('default' => 1);
    $options['css'] = array('default' => 1);
    $options['render_tag'] = array('default' =>'ul');
    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Flatten options to deal with the various hierarchy changes.
    $options = views_omega_grid_get_options($this->options);

    $form['grid']= array(
      '#type' => 'fieldset',
      '#title' => t('Grid Configuration'),
      '#weight' => 70,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['wrapper_classes'] = array(
      '#type' => 'textfield',
      '#title' => t('Wrapper Classes'),
      '#default_value' => $options['wrapper_classes'],
      '#description' => t('Enter a wrapper class.'),
      '#fieldset' => 'grid',
      '#weight' => -71,
    );
    // is this a views bug?
    $form['grid']['wrapper_classes']['#weight'] = -71;
    $form['default_classes'] = array(
      '#type' => 'checkbox',
      '#title' => t('Remove default wrapper classes'),
      '#default_value' => $options['default_classes'],
      '#fieldset' => 'grid',
      '#weight' => -70,
    );
    // is this a views bug?
    $form['grid']['default_classes']['#weight'] = -70;
    $form['render_tag'] = array(
      '#type' => 'radios',
      '#title' => t('Tag to render'),
      '#options' => array('ul' => t('Unordered list'), 'div' => t('Container')),
      '#default_value' => $this->options['render_tag'],
      '#fieldset' => 'grid',
      '#weight' => -69,
    );
    // is this a views bug?
    $form['grid']['render_tag']['#weight'] = -69;
    $form['row_class']['#default_value'] = $options['row_class'];
    $form['row_class']['#fieldset'] = 'grid';

    $form['switch'] = array(
      '#type' => 'checkbox',
      '#title' => t('Grid / List Switch.'),
      '#default_value' => $options['switch'],
      '#description' => t('Adds a Switch Link to disable / enable the grid.'),
      '#fieldset' => 'grid',
    );
    $form['css'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use the default Grid'),
      '#default_value' => $options['css'],
      '#description' => t('Use the default responsive grid provided by this module.'),
      '#fieldset' => 'grid',
    );
  }
}
