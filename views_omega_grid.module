<?php
/**
* @file
* Provides a Views style plugin to display content in a responsive grid.
*/

/**
 * Implements hook_views_api().
 */
function views_omega_grid_views_api() {
  return array('api' => '3.0');
}

/**
 * Set default options.
 */
function views_omega_grid_get_options($array) {
  $options = array();
  foreach ($array as $key => $value) {
    if (!is_array($value)) {
      $options[$key] = $value;
    }
    else {
      $options = array_merge($options, views_omega_grid_get_options($value));
    }
  }
  return $options;
}
/**
 * Implements template_preprocess_views_view().
 */
function views_omega_grid_preprocess_views_view(&$vars) {
  $view = $vars['view'];

  $switch = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => ('grid-switcher'),
    ),
    'grid_on' => array(
      '#theme' => 'link',
      '#text' => t('Show Grid View'),
      '#path' => '',
      '#options' => array(
        'attributes' => array(
          'class' => array('js-grid-on'),
        ),
        'html' => FALSE,
        'fragment' => FALSE,
        'external' => TRUE,
      ),
    ),
    'spacer' => array(
      '#type' => 'markup',
      '#markup' => ' ',
    ),
    'list_on' => array(
      '#theme' => 'link',
      '#text' => t('Show List View'),
      '#path' => '',
      '#options' => array(
        'attributes' => array(
          'class' => array('js-list-on'),
        ),
        'html' => FALSE,
        'fragment' => FALSE,
        'external' => TRUE,
      ),
    ),
  );
  $switch_output = drupal_render($switch);

  if ($view->style_plugin->plugin_name == 'omega_grid' && $view->style_plugin->options['switch']) {
    !empty($view->exposed_widgets) ? $vars['exposed'] .= $switch_output : $vars['exposed'] = $switch_output;
  }
}
/**
 * Display a view as a responsive grid style.
 */
function template_preprocess_views_view_omega_grid(&$vars) {
  $view = $vars['view'];

  $handler = $view->style_plugin;

  $options = views_omega_grid_get_options($view->style_plugin->options);
  $default_row_class = isset($options['default_row_class']) ? $options['default_row_class'] : TRUE;
  $row_class_special = isset($options['row_class_special']) ? $options['row_class_special'] : TRUE;

  if ($options['default_classes']) {
    $vars['classes_array'] = array();
  }
  $vars['classes_array'] = array_merge($vars['classes_array'], explode(' ', $options['wrapper_classes']));
  $vars['classes_array'] = array_filter($vars['classes_array']);

  $vars['classes'] = implode(' ', $vars['classes_array']);

  $count = 0;
  foreach ($vars['rows'] as $num => $row) {
    $vars['row_classes'][$num] = array();
    if ($default_row_class) {
      $vars['row_classes'][$num][] =  'views-row-' . ($num + 1);
    }
    if ($row_class_special) {
      $vars['row_classes'][$num][] = ($count++ % 2 == 0) ? 'odd' : 'even';
    }
  }

  if ($row_class_special) {
    $vars['row_classes'][0][] = 'views-row-first';
    $vars['row_classes'][count($vars['row_classes']) - 1][] = 'views-row-last';
  }

  if (isset($options['row_class'])) {
    foreach ($vars['rows'] as $num => $row) {
      if ($row_class = $handler->get_row_class($num)) {
        $vars['row_classes'][$num] = array_merge($vars['row_classes'][$num], explode(' ',$row_class));
      }
    }
  }
  $render_tag = $options['render_tag'];
  switch ($render_tag) {
    case 'ul':
      $wrapper = 'ul';
      $item_list = 'li';
      break;
    case 'div':
      $wrapper = 'div';
      $item_list = 'div';
  };
  $vars['wrapper'] = $wrapper;
  $vars['item_list'] = $item_list;
}
